/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.swing1;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author focus
 */
public class Frame1 {

    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//*(ห้ามลืม) กำหนดให้ netbean หยุดรัน โปรแกรมต่อเมื่อกดปิดโปรแกรม
        //frame.setSize(new Dimension(500, 300));// กำหนดขนาด JFrame
        frame.setSize(500, 300);// เป็น Overloading method

        JLabel lblHelloWorld = new JLabel("Hello World!!",JLabel.CENTER);//เป็นข้อความที่ไม่สามารถทำอะไรกับมันได้ และเป็น(Overloading Method)
        lblHelloWorld.setBackground(Color.PINK);//SetBackground แต่จะยังไม่ShowBG
        lblHelloWorld.setOpaque(true);//แสดงสีของ BG ที่กำหนด
        lblHelloWorld.setFont(new Font("Verdana", Font.BOLD, 25));//กำหนดฟรอนต์ และ ขนาด

        frame.add(lblHelloWorld);// add ... เข้าไปใน JFrame Default อยู่ทางด้านซ้าน

        //อยากให้ JFrame ทำงาน
        frame.setVisible(true); //Set = กำหนดค่า Get = นำค่าที่กำหนดออกมา

    }

}
